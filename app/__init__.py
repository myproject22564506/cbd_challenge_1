from flask import Flask, render_template, jsonify

app = Flask(__name__)

api_token = "MySecureToken"
api_url = "https://fakeweatherservice.com/getforecast"

@app.route('/')
def home():
	return render_template('home.html', name="LIYA ACCAMMA SHAJEE")

@app.route('/dashboard')
	def dashboard():
		weather_data = {
			"API_TOKEN": api_token,
			"API_URL": api_url
		}
		return jsonify(weather_data)

if __name__ == '__main__':
	app.run(debug=True)
